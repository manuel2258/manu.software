+++
title = "gitblick"
description = "A git metrics tracking and visualization tool"
date = "2024-04-21"
[taxonomies]
technologies = ["rust", "git"]
[extra]
badges = [
  "https://img.shields.io/gitlab/contributors/manuel2258%2Fgitblick?style=for-the-badge",
  "https://img.shields.io/gitlab/stars/manuel2258%2Fgitblick?style=for-the-badge", 
  "https://img.shields.io/gitlab/forks/manuel2258%2Fgitblick?style=for-the-badge", 
]
source = "https://gitlab.com/manuel2258/gitblick"
status = "abandoned"
+++

Gitblick is a tool to tack data about code (such as code coverage, linting scores ...) for each commit. The tracked
data can then be visualized using svg graphics and included in the git README file.

## Example

The repository [gitlab.com/manuel2258/gitblick_test](https://gitlab.com/manuel2258/gitblick_test) is a test repository
which uses gitblick in its CI/CD pipeline. It can generate following visualization:

![head](https://gitlab.com/manuel2258/gitblick_test/-/raw/_gitblick/output/head.svg)

using following gitlab CI/CD configuration:

```yaml
image: registry.gitlab.com/manuel2258/gitblick/gitblick:staging
stages:
  - append
  - render
append-test_int-1:
  stage: append
  script:
      # Tracks the value '1' under the name 'test_int' to the current commit
    - gitblick --http-password $GITBLICK_GIT_HTTP_PASSWORD append -n test_int -v 1 
render-all:
  stage: render
  script:
      # Renders all the graphics
    - gitblick --http-password $GITBLICK_GIT_HTTP_PASSWORD render-all
```


## Implementation

Gitblick works by storing configuration and tracked data in a special branch named `_gitblick`.  
This is an example from the `gitblick_test` repository:

```sh
├── gitblick.ron
├── output
│   └── head.svg
├── records
│   ├── 0952789ba825b2ecc42ec6f40b4166743b754276
│   │   └── test_int
...
│   └── f7846bd0221b3964576ecfe7e0df6b2c8af36e36
│       └── test_int
└── templates
    └── head.svg
```

There are input files, such as the `gitblick.ron` configuration file as well as the `templates` folder, which holds
the tera svg templates files.  
Additionally stored on the same branch are the output files, which contain the actual records as well as the output graphics.
The records are stored in the folder named after the origin commit hash, and then stored in a file with the point name.

## Configuration

The config file is in the ron format, any mainly defines the point name and types, as well as the to render outputs.

```ron
Config (
  project: ProjectConfig(
    name: "gitblick_test",
  ),
  points: {
    "test_string": String,
    "test_int": Integer,
  },
  outputs: [
    Tera(TeraOutput(
      source: "templates/**/*",
      destination: "output",
    )),
  ]
)
```

## Run options


```sh
|> gitblick git:(master) gitblick --help
A git metrics tracking and visualization tool

Usage: gitblick [OPTIONS] [COMMAND]

Commands:
  append      
  list        
  render-all  
  checkout    
  help        Print this message or the help of the given subcommand(s)

Options:
  -c, --config <CONFIG>                    
  -r, --repo <REPO>                        
      --remote <REMOTE>                    
  -d, --dry-run                            
      --auth-username <AUTH_USERNAME>      [default: git]
      --http-password <HTTP_PASSWORD>      
      --ssh-private-key <SSH_PRIVATE_KEY>  
      --ssh-passphrase <SSH_PASSPHRASE>    
  -h, --help                               Print help
  -V, --version                            Print version
```


## Abandoning Reason

Unfortunately, it is not easily possible to template svg files, as it has no layouting such as html. Meaning when for
example iterating through a list of points, you would have to have a variable, which you manually increment to track
the y position of those points.
Being able to generate output artifacts by embedding a scripting language such a python would be the nice solution,
but that is too much work for me right now ...
