+++
title = "spacemd"
description = "1vs1 tower defense game"
date = "2023-11-01"
[taxonomies]
technologies = ["rust", "bevy"]
[extra]
badges = [
  "https://img.shields.io/gitlab/contributors/manuel2258%2Fspacemd?style=for-the-badge",
  "https://img.shields.io/gitlab/stars/manuel2258%2Fspacemd?style=for-the-badge", 
  "https://img.shields.io/gitlab/forks/manuel2258%2Fspacemd?style=for-the-badge", 
]
source = "https://gitlab.com/manuel2258/spacemd"
status = "abandoned"
+++

### Tech
A multiplayer 1vs1 tower defense game, written in rust using the bevy game engine.
Both server and client are written using bevy and renet as a transport library.

### State
The game is in a very early stage, mostly containing basic client functionality.

### Gameplay
The goal of the game was to build up your space station, consisting mainly of turrets.
Each turret can either attack the enemy, defend against attacks or mine meteors.

The player then continues building up his arsenal while attacking the enemy.
The main inspiration is [legiontd2](https://beta.legiontd2.com/).

