+++
title = "fafb"
description = "2d platformer game"
date = "2024-02-01"
[taxonomies]
technologies = ["rust", "bevy"]
[extra]
badges = [
  "https://img.shields.io/gitlab/contributors/manuel2258%2Ffafb?style=for-the-badge",
  "https://img.shields.io/gitlab/stars/manuel2258%2Ffafb?style=for-the-badge", 
  "https://img.shields.io/gitlab/forks/manuel2258%2Ffafb?style=for-the-badge", 
]
source = "https://gitlab.com/manuel2258/fafb"
status = "abandoned"
+++

### Tech
A singleplayer platformer written in rust using bevy.

### State
The game is in a very early stage, mostly containing a player controller.

### Gameplay
The idea was to create a souls-like platformer similar to [Hollow Knight](https://www.hollowknight.com/).
