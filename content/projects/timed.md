+++
title = "timed"
description = "A physics puzzle game involving time manipulation"
date = "2024-07-01"
[taxonomies]
technologies = ["rust", "rapier", "notan"]
[extra]
badges = []
status = "ongoing"
+++

## Gameplay

Timed is a physics puzzle game, where your goal is to define events at the right moment to get objects
into there right place.  
It allows you to freely jump around to every timestep in the physics simulation and adjust the environment at that
given time point.  

## Technology

It is a rewrite of a game I have already partially written in Unity game engine several years ago, where I was greatly
limited by the performance of the physics engine.  
So this time I'm fully writing it from scratch, using rust, the [rapier](https://www.rapier.rs/) physics engine and a
simple multimedia layer [notan](https://github.com/Nazariglez/notan).

## Open Source
The game is not yet open source and more of a fun projects without the serious intention of actually releasing it
someday.
