+++
title = "mirage"
description = "SSO management platform"
date = "2022-06-01"
[taxonomies]
technologies = ["elixir", "phoenix"]
[extra]
badges = [
  "https://img.shields.io/gitlab/contributors/oecis%2Fmirage?style=for-the-badge",
  "https://img.shields.io/gitlab/stars/oecis%2Fmirage?style=for-the-badge", 
  "https://img.shields.io/gitlab/forks/oecis%2Fmirage?style=for-the-badge", 
]
logo = "https://gitlab.com/uploads/-/system/project/avatar/45284104/DALL_E_2023-04-18_20.20.41_1_.jpg"
source = "https://gitlab.com/oecis/mirage"
homepage = "https://oecis.io"
status = "on hold"
+++

A custom frontend for [kratos](https://www.ory.sh/kratos/) using [elixir](https://elixir-lang.org/) and [phoenix](https://www.phoenixframework.org/).  

## features
### kratos workflows
- login
- logout
- register
- verification
- recovery
- oauth2/consent
### automatic plugs
- csrf extraction
- session / user data extraction

