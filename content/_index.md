+++
template = "simple_page.html"
+++

# Welcome to Manu's personal website

I'm a software developer from Germany, working with all kinds of weird technologies.  
You can mainly find an overview of all my projects here.
