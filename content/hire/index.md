+++
title = "hire"
template = "simple_page.html"
draft = true
+++

# It's your lucky day
## I'm up for hiring right now!

If you are looking for a developer in either
  - Rust
  - Elixir
  - C++
  - Python
  - any other interesting technology matching my CV

and think your company or project could interest me?

Then feel free to contact me!

<div class="flex space-x-4">
  <a class="basis-1/2" href="/hire/Manuel_Schmidbauer_CV_public.pdf">
    <button class="btn btn-outline btn-primary text-2xl xl:text-lg h-20 xl:h-14 border-4 xl:border-2">
      Download CV
    </button>
  </a>
  <a class="basis-1/2" href="mailto:development@manu.software">
    <button class="btn btn-outline btn-primary text-2xl xl:text-lg h-20 xl:h-14 border-4 xl:border-2">
      development@manu.software
    </button>
  </a>
</div>
