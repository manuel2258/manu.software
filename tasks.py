from invoke import task, Collection
from time import sleep


@task
def serve_and_watch(c, ip="127.0.0.1"):
    c.run(
        "yarn run tailwindcss -i styles/styles.css -o static/main.css --minify --watch",
        asynchronous=True,
        hide=None,
    )
    c.run(f"zola serve -i {ip} -u {ip}")


@task
def serve_stop(c):
    c.run("pkill -fe -9 '.*zola.*serve.*'", warn=True)
    sleep(0.1)
    c.run("pkill -fe -9 '.*inv.*serve.start.*'", warn=True)


serve = Collection("serve")
serve.add_task(serve_and_watch, "start")
serve.add_task(serve_stop, "stop")
ns = Collection()
ns.add_collection(serve)
