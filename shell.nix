
{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.zola
    pkgs.yarn
    pkgs.python311Packages.invoke
  ];
}

